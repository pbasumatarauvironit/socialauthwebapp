package com.vironit.petproject.socialauth.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
public class NewAccountDto {

    @Email
    @NotBlank
    private final String email;

    @NotBlank
    private final String rawPassword;

    @JsonCreator
    public NewAccountDto(@JsonProperty("email") String email,
                         @JsonProperty("rawPassword") String rawPassword) {
        this.email = email;
        this.rawPassword = rawPassword;
    }
}
