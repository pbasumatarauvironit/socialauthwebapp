package com.vironit.petproject.socialauth.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
public class LoginDto {

    @Email
    @NotBlank
    private final String email;

    @NotBlank
    private final String rawPassword;

    @JsonCreator
    public LoginDto(@JsonProperty("email") String email,
                    @JsonProperty("rawPassword") String rawPassword) {
        this.email = email;
        this.rawPassword = rawPassword;
    }
}
