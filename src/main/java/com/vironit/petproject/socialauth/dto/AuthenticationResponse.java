package com.vironit.petproject.socialauth.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
public class AuthenticationResponse {

    @NotBlank
    private String accessToken;

    private final String tokenType = "Bearer";

    public AuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
