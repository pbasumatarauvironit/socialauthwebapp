package com.vironit.petproject.socialauth.model;

import lombok.*;

import javax.persistence.*;

@Entity
@DiscriminatorValue(value = Identity.TYPE_PERSON)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class Person extends Identity{
    private String name;
    private String givenName;
    private String familyName;
}
