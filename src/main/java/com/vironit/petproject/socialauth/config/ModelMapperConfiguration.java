package com.vironit.petproject.socialauth.config;

import com.vironit.petproject.socialauth.converter.NewUserAccountToUser;
import com.vironit.petproject.socialauth.converter.UserToCustomUserDetails;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
    public ModelMapper modelMapper() {
        final ModelMapper modelMapper = new ModelMapper();

        modelMapper.addConverter(new UserToCustomUserDetails());
        modelMapper.addConverter(getNewUserAccountToUserConverter());
        return modelMapper;
    }

    @Bean
    public NewUserAccountToUser getNewUserAccountToUserConverter(){
        return new NewUserAccountToUser();
    }

}
