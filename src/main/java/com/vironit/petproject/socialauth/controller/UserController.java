package com.vironit.petproject.socialauth.controller;

import com.vironit.petproject.socialauth.dto.AuthenticationResponse;
import com.vironit.petproject.socialauth.dto.LoginDto;
import com.vironit.petproject.socialauth.dto.NewAccountDto;
import com.vironit.petproject.socialauth.security.CurrentUser;
import com.vironit.petproject.socialauth.security.CustomUserDetails;
import com.vironit.petproject.socialauth.security.JwtTokenProvider;
import com.vironit.petproject.socialauth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @PostMapping(value = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void registerNewUser(@Valid @RequestBody NewAccountDto newAccount){
        userService.registerNewAccount(newAccount);
    }

    //todo fix this shit if possible...
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> login(@Valid @RequestBody LoginDto dto){

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        dto.getEmail(),
                        dto.getRawPassword()
                )
        );

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthenticationResponse(token));
    }

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> login(@PathParam(value = "token") String token){
        tokenProvider.validate(token);

        return ResponseEntity.ok(new AuthenticationResponse(token));
    }

    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public CustomUserDetails appUserInfo(@CurrentUser CustomUserDetails userDetails){
        return userDetails;
    }

}
