package com.vironit.petproject.socialauth.security.impl;

import com.vironit.petproject.socialauth.model.User;
import com.vironit.petproject.socialauth.repository.UserRepository;
import com.vironit.petproject.socialauth.security.CustomUserDetails;
import com.vironit.petproject.socialauth.security.CustomUserDetailsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        final User user = userRepository
                .findByEmail(name)
                .orElseThrow(() -> new UsernameNotFoundException("not found user by name: " + name));

        return convertToCustomUserDetails(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomUserDetails> loadUserByUserId(Long id) throws EntityNotFoundException {
        return userRepository
                .findById(id)
                .map(this::convertToCustomUserDetails);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomUserDetails> loadUserByEmail(String email) throws EntityNotFoundException {
        return userRepository
                .findByEmail(email)
                .map(this::convertToCustomUserDetails);
    }

    @Override
    public CustomUserDetails getDetailsForAuthenticatedUser(User user) {
        return convertToCustomUserDetails(user);
    }

    private CustomUserDetails convertToCustomUserDetails(User user){
        return modelMapper.map(user, CustomUserDetailsImpl.class);
    }

}
