package com.vironit.petproject.socialauth.security;

import com.vironit.petproject.socialauth.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

public interface CustomUserDetailsService
        extends UserDetailsService {

    Optional<CustomUserDetails> loadUserByUserId(Long id) throws EntityNotFoundException;
    Optional<CustomUserDetails> loadUserByEmail(String email) throws EntityNotFoundException;
    CustomUserDetails getDetailsForAuthenticatedUser(User user);
}
