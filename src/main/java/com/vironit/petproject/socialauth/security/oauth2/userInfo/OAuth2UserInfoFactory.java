package com.vironit.petproject.socialauth.security.oauth2.userInfo;

import java.util.Map;

import static com.vironit.petproject.socialauth.model.AuthenticationDetails.AuthenticationProvider.*;

public class OAuth2UserInfoFactory {

    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        switch (valueOf(registrationId)){
            case google:
                return new GoogleOAuth2UserInfo(attributes);
            case github:
                return new GithubOAuth2UserInfo(attributes);
            default:
                throw new RuntimeException("registrationId not allowed (no client registration details provided)");
        }
    }
}
