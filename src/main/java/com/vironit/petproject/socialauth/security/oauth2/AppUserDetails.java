package com.vironit.petproject.socialauth.security.oauth2;

import org.springframework.security.core.userdetails.UserDetails;

public interface AppUserDetails extends UserDetails {

}
