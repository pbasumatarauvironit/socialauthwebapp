package com.vironit.petproject.socialauth.security.oauth2;

import com.vironit.petproject.socialauth.model.AuthenticationDetails;
import com.vironit.petproject.socialauth.model.AuthenticationDetails.AuthenticationProvider;
import com.vironit.petproject.socialauth.model.User;
import com.vironit.petproject.socialauth.repository.UserRepository;
import com.vironit.petproject.socialauth.security.CustomUserDetails;
import com.vironit.petproject.socialauth.security.CustomUserDetailsService;
import com.vironit.petproject.socialauth.security.exception.Oauth2AuthenticationProcessingException;
import com.vironit.petproject.socialauth.security.oauth2.userInfo.OAuth2UserInfo;
import com.vironit.petproject.socialauth.security.oauth2.userInfo.OAuth2UserInfoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);
        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    @Transactional
    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        String registrationId = oAuth2UserRequest.getClientRegistration().getRegistrationId();

        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(
                registrationId, oAuth2User.getAttributes()
        );

        if(StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new Oauth2AuthenticationProcessingException("Authentication authority doesn't have the user email, " +
                    "auth id: " + registrationId);
        }
        AuthenticationProvider authProviderIdFromRequest =
                AuthenticationProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId());

        CustomUserDetails details;
        final Optional<CustomUserDetails> userDetails =
                customUserDetailsService.loadUserByEmail(oAuth2UserInfo.getEmail());

        if(userDetails.isPresent()){
            details = userDetails.get();
            if(!details.getRegistrationId().equals(authProviderIdFromRequest.toString())){
                throw new Oauth2AuthenticationProcessingException("Looks like you're signed up with " +
                        authProviderIdFromRequest.toString() + " account. Please use your " +
                        details.getRegistrationId() + " to sign in.");
            }

            User retrievedUser = userRepository.findByEmail(details.getEmail())
                    .orElseThrow(() -> new EntityNotFoundException("no user found by email: " + details.getEmail()));

            updateUserWithFreshDetails(retrievedUser, oAuth2UserInfo);
        }else{
            final User persistedUser =
                    userRepository.save(
                            convertOauth2UserIfoToUserAndPersistNewAccount(oAuth2UserInfo, authProviderIdFromRequest)
                    );

            details = customUserDetailsService.getDetailsForAuthenticatedUser(persistedUser);
        }
        return details;
    }

    private User convertOauth2UserIfoToUserAndPersistNewAccount(OAuth2UserInfo userInfo,
                                                                AuthenticationProvider authProviderIdFromRequest){

        final AuthenticationDetails authDetails = AuthenticationDetails.transientBuilder()
                .providedId(userInfo.getId())
                .authProvider(authProviderIdFromRequest)
                .imageUrl(userInfo.getImageUrl())
                .attributes(userInfo.getAttributes())
                .build();

        return User.userBuilder()
                .name(userInfo.getName())
                .email(userInfo.getEmail())
                .role(User.Role.USER)
                .credentialsNonExpired(true)
                .accountNonLocked(true)
                .accountConfirmed(true)
                .accountNonExpired(true)
                .enabled(true)
                .lastActive(new Date().toInstant().getEpochSecond())
                .authDetails(authDetails)
                .build();
    }

    private void updateUserWithFreshDetails(User user, OAuth2UserInfo userInfo) {
        //todo update user with fresh data from auth provider
    }
}
