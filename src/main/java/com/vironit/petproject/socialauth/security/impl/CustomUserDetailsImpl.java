package com.vironit.petproject.socialauth.security.impl;

import com.vironit.petproject.socialauth.security.CustomUserDetails;
import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;

@Builder
public class CustomUserDetailsImpl implements CustomUserDetails {

    private Boolean accountNonLocked;
    private Boolean accountNonExpired;
    private Boolean credentialsNonExpired;
    private Boolean accountConfirmed;

    private Long id;

    private Boolean enabled;
    private String email;
    private String userName;
    private String passwordHash;
    private String registrationId;

    private Collection<? extends GrantedAuthority> authorities;
    private Map<String, Object> attributes;

    @Override
    public String getEmail() {
        return email;
    }

    //client registered at authentication provider under a particular set of credentials (id and secret)
    //the credentials are stored at client side as well under the registration id
    @Override
    public String getRegistrationId() {
        return registrationId;
    }

    //id of the authenticated user principal
    @Override
    public String getName() {
        return String.valueOf(id);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public String getPassword() {
        return passwordHash;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
