package com.vironit.petproject.socialauth.converter;

import com.vironit.petproject.socialauth.dto.NewAccountDto;
import com.vironit.petproject.socialauth.model.AuthenticationDetails;
import com.vironit.petproject.socialauth.model.User;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.vironit.petproject.socialauth.model.AuthenticationDetails.AuthenticationProvider.*;

@Component
public class NewUserAccountToUser implements Converter<NewAccountDto, User> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User convert(MappingContext<NewAccountDto, User> mappingContext) {
        if (mappingContext.getSource() == null){
            return null;
        }

        final AuthenticationDetails details = AuthenticationDetails.builder()
                .authProvider(local)
                .build();

        return User.userBuilder()
                .accountNonExpired(true)
                //todo fix confirmation
                .accountConfirmed(true)
                .accountNonLocked(true)
                //todo fix confirmation
                .enabled(true)
                .authDetails(details)
                .lastActive(new Date().toInstant().getEpochSecond())
                .role(User.Role.USER)
                .credentialsNonExpired(true)
                .email(mappingContext.getSource().getEmail())
                .passwordHash(passwordEncoder.encode(mappingContext.getSource().getRawPassword()))
                .build();
    }
}
