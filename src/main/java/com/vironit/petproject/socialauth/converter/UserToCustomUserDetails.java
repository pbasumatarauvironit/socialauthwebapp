package com.vironit.petproject.socialauth.converter;

import com.vironit.petproject.socialauth.model.User;
import com.vironit.petproject.socialauth.security.impl.CustomUserDetailsImpl;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;

public class UserToCustomUserDetails implements Converter<User, CustomUserDetailsImpl> {
    @Override
    public CustomUserDetailsImpl convert(MappingContext<User, CustomUserDetailsImpl> mappingContext) {

        return mappingContext.getSource() == null ? null :
                CustomUserDetailsImpl.builder()
                        .accountConfirmed(mappingContext.getSource().getAccountConfirmed())
                        .accountNonExpired(mappingContext.getSource().getAccountNonExpired())
                        .accountNonLocked(mappingContext.getSource().getAccountNonLocked())
                        .credentialsNonExpired(mappingContext.getSource().getCredentialsNonExpired())
                        .email(mappingContext.getSource().getEmail())
                        .enabled(mappingContext.getSource().getEnabled())
                        .passwordHash(mappingContext.getSource().getPasswordHash())
                        .attributes(mappingContext.getSource().getDetails().getAttributes())
                        .id(mappingContext.getSource().getId())
                        .authorities(Collections.singletonList(new SimpleGrantedAuthority(mappingContext.getSource().getRole().name())))
                        .userName(mappingContext.getSource().getEmail())
                        .registrationId(mappingContext.getSource().getDetails().getAuthProvider().name())
                        .build();
    }
}
