package com.vironit.petproject.socialauth.service;

import com.vironit.petproject.socialauth.dto.NewAccountDto;

public interface UserService {
    void registerNewAccount(NewAccountDto dto);
}
