package com.vironit.petproject.socialauth.service.impl;

import com.vironit.petproject.socialauth.dto.NewAccountDto;
import com.vironit.petproject.socialauth.exception.UserAccountOccupied;
import com.vironit.petproject.socialauth.model.User;
import com.vironit.petproject.socialauth.repository.UserRepository;
import com.vironit.petproject.socialauth.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void registerNewAccount(NewAccountDto dto) {
        if(userRepository.findByEmail(dto.getEmail()).isPresent()){
            throw new UserAccountOccupied("there is already registered user with email: " + dto.getEmail());
        }
        userRepository.save(modelMapper.map(dto, User.class));
    }
}
