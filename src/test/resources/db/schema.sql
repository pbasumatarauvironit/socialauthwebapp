DROP SCHEMA if exists social_app_db_schema CASCADE;

CREATE SCHEMA if not exists social_app_db_schema AUTHORIZATION postgres;

COMMENT ON SCHEMA social_app_db_schema IS 'test db for persistence tests ';

-- Drop table

-- DROP TABLE social_app_db_schema.externalids;

create table social_app_db_schema.externalids (
	uuid uuid not null,
	CONSTRAINT externalids_pk PRIMARY KEY (uuid)
);

-- DROP SEQUENCE social_app_db_schema.users_id_seq;

create sequence social_app_db_schema.users_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 3
	CACHE 1
	NO CYCLE;

-- Drop table

-- DROP TABLE social_app_db_schema.users;

create table social_app_db_schema.users (
	id bigserial not null,
	uuid uuid not null,
	"type" varchar(12) not null,

	"role" varchar(16) not null DEFAULT 'USER'::character varying,
	"name" varchar(60) not null,
	givenname varchar(150) null,
	familyname varchar(150) null,

	enabled bool not null,
	accountnonlocked bool not null,
	accountnonexpired bool not null,
	credentialsnonexpired bool not null,
	accountconfirmed bool not null,
	email varchar(160) not null,
	lastactive bigint not null,
	passwordhash varchar(160) null,

	authprovider varchar(80) not null,
	providedid varchar(120) null,
	imageurl varchar(160) null,
	CONSTRAINT users_pk PRIMARY KEY (id)
);
create unique index users_email_idx ON social_app_db_schema.users USING btree (email);
alter table social_app_db_schema.users ADD CONSTRAINT users_fk FOREIGN KEY (uuid) references externalids(uuid) ON UPDATE cascade ON DELETE cascade;



