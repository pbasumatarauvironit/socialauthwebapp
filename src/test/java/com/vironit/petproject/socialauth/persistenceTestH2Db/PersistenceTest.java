package com.vironit.petproject.socialauth.persistenceTestH2Db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(value = "persistence-test-h2-db")
public abstract class PersistenceTest {

    @Test
    public void contextLoads() {
    }
}
