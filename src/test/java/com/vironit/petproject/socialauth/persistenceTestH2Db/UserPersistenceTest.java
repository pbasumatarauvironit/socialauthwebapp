package com.vironit.petproject.socialauth.persistenceTestH2Db;

import com.vironit.petproject.socialauth.model.AuthenticationDetails;
import com.vironit.petproject.socialauth.model.Identity;
import com.vironit.petproject.socialauth.model.User;
import com.vironit.petproject.socialauth.repository.UserRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class UserPersistenceTest extends PersistenceTest {

    @Autowired
    private UserRepository userRepository;

    private static PasswordEncoder passwordEncoder;

    @BeforeClass
    public static void init(){
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @Test
    public void whenUserUserPersisted_thenTheUserRetrievedCorrectly(){
        AuthenticationDetails details = AuthenticationDetails.builder().authProvider(
                AuthenticationDetails.AuthenticationProvider.google)
                .providedId("someProvidedId").build();

        User savedUser = User.userBuilder()
                .accountConfirmed(true)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .email("j.doe@gmail.com")
                .enabled(true)
                .lastActive(new Date().toInstant().getEpochSecond())
                .passwordHash(passwordEncoder.encode("stub"))
                .familyName("Doe")
                .givenName("John")
                .name("John Doe")
                .authDetails(details)
                .build();

        userRepository.save(savedUser);

        User retrievedUser = userRepository.findByEmail(
                savedUser.getEmail()).orElseThrow(() -> new RuntimeException("no entity found")
        );

        assertThat(retrievedUser.getEmail()).isEqualTo(savedUser.getEmail());
        assertThat(retrievedUser.getAccountConfirmed()).isEqualTo(savedUser.getAccountConfirmed());
        assertThat(retrievedUser.getAccountNonExpired()).isEqualTo(savedUser.getAccountNonExpired());
        assertThat(retrievedUser.getAccountNonLocked()).isEqualTo(savedUser.getAccountNonLocked());
        assertThat(retrievedUser.getEnabled()).isEqualTo(savedUser.getEnabled());
        assertThat(retrievedUser.getLastActive()).isEqualTo(savedUser.getLastActive());
        assertThat(retrievedUser.getPasswordHash()).isEqualTo(savedUser.getPasswordHash());
        assertThat(retrievedUser.getFamilyName()).isEqualTo(savedUser.getFamilyName());
        assertThat(retrievedUser.getGivenName()).isEqualTo(savedUser.getGivenName());
        assertThat(retrievedUser.getName()).isEqualTo(savedUser.getName());
        assertThat(retrievedUser.getId()).isNotNull();
        assertThat(retrievedUser.getExternalId().getUuid()).isNotNull();

        System.out.print("retrieved external id: " + retrievedUser.getExternalId());
    }
}


