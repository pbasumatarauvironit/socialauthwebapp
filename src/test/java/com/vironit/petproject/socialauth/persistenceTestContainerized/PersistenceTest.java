package com.vironit.petproject.socialauth.persistenceTestContainerized;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.*;
import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(value = "persistence-test-containerized")
public abstract class PersistenceTest {

    private static DockerComposeContainer env;

    //issue with volume sharing (docker runs on windows)

    //invoked before test classes assembly
    @ClassRule
    public static ExternalResource setup() {
        return new ExternalResource() {
            @Override
            protected void before() throws Throwable {
                final URL resource = PersistenceTest.class.getClassLoader()
                        .getResource("docker-compose-persistence-test-env.yml");

                assert resource != null;

                env = new DockerComposeContainer(new File(resource.toURI()))
                        .withExposedService("postgres-it", 54320);

                env.start();
            }

            @Override
            protected void after() {
                env.stop();
            }
        };
    }

    @Test
    public void contextLoads() {
    }
}
